## [1.2.6]

> 2019-07-19

* блочим кнопку fullscreen, если liveStreamOnly = true

## [1.2.5]

> 2019-06-20

* скрыта кнопка live в полноэкранном режиме 

## [1.2.4]

> 2019-04-26

* добавлен новый prop liveStreamOnly

## [1.2.3]

> 2019-04-12

* Изменен тултип кнопки клипов
* Пофикшена ошибка с запросом emit key frame

## [1.2.2]

> 2019-04-11

* Выводим в console.info версию виджета 'SH_WIDGET_PLAYER: [version]' 
* Минимальная ширина плеера изменена на 800px

## [1.2.0]

> 2019-01-29

* Добавлена возможность передать в props функцию onVideoBakerCallback 
 

## [1.1.0]

> 2018-12-18

* Добавлена возможность передать в props:
  - session объект для запросов 
  - proxyUrl - String префикс для запросов

## [1.0.0]

> 2018-08-17

* Первоначальная сборка