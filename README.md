# Виджет плеера для УД

### Параметры, которые передаются виджету плеера для УД

- uid - String - uid камеры
- start - String - таймштамп в милисекундах, который начнет проигрываться в плеере
- proxyUrl - String - префикс для запросов
- liveStreamOnly - Boolean - Если true, то блокируется весь функционал плеера, кроме play/pause
- onVideoBakerCallback - Function - callback после запроса на выпечку клипа, возвращает { type: 'success' } или { type: 'error', message: 'string' }
- session: Объект сессии - PropTypes.Shape
  - 'X-User-Key': PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  - 'X-User-Token': PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  - 'HTTP_HOME_ID': PropTypes.oneOfType([PropTypes.string, PropTypes.number]) 